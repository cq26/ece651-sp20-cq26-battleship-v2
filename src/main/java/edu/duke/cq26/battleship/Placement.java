package edu.duke.cq26.battleship;

import java.util.Locale;

/**
 * @program :       battleship
 * @description:    Class of Placement, which is to do placement on board
 **/
public class Placement {
    private final Coordinate coordinate;
    private final char orientation;

    /**
     * @Description: Default constructor of Placement. Constructed by the passed coordinate and the passed orientation
     * @Param:       Coordinate coordinate, char orientation
     * @throws       IllegalArgumentException if the width or height is smaller than 0.
     * @return:
     **/
    public Placement(Coordinate coordinate, char orientation) {
        this.coordinate = coordinate;
        this.orientation = orientation;
    }

    /**
     * @Description: Constructor of Placement. Constructed by the string description like "A0V"
     * @Param:       String descr
     * @throws       IllegalArgumentException if the description's length is not 3 or orientation is not 'H'/'V'.
     * @return:
     **/
    public Placement(String descr){
        if(descr.length() != 3){
            throw new IllegalArgumentException("edu.duke.cq26.battleship.Placement's length must be 3\n");
        }

        descr = descr.toUpperCase(Locale.ROOT);

        char orientaion = descr.charAt(2);

        if(orientaion != 'H' && orientaion != 'V' && orientaion != 'U' && orientaion != 'R' && orientaion != 'D' && orientaion != 'L'){
            throw new IllegalArgumentException("edu.duke.cq26.battleship.Placement's orientation must be V/H\n");
        }
        this.coordinate = new Coordinate(descr.substring(0,2));
        this.orientation = orientaion;
    }

    /** getter and setters **/
    public Coordinate getCoordinate() {
        return coordinate;
    }

    public char getOrientation() {
        return orientation;
    }

    /**
     * @Description: Check if the passed object is an Object of Placement, and check if it has same value with "this".
     * @Param:       Object o
     * @return:      Boolean that indicate if the passed Object is equal to "this".
     **/
    @Override
    public boolean equals(Object o) {
        if (o.getClass().equals(getClass())) {
            Placement p = (Placement) o;
            return coordinate.equals(p.coordinate) && orientation == p.orientation;
        }
        return false;
    }

    /**
     * @Description: Describe this Placement in String.
     * @Param:
     * @return:      String that describe this Placement
     **/
    @Override
    public String toString() {
        return "("+coordinate.toString()+", " + orientation+")";
    }

    /**
     * @Description: Calculate the hashcode of this.toString()
     * @Param:
     * @return:      Int of hashcode
     **/
    @Override
    public int hashCode() {
        return toString().hashCode();
    }

}
