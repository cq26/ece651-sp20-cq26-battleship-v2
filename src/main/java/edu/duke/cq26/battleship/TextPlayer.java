package edu.duke.cq26.battleship;

import edu.duke.cq26.battleship.interfaces.AbstractShipFactory;
import edu.duke.cq26.battleship.interfaces.Board;
import edu.duke.cq26.battleship.interfaces.Ship;

import java.io.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Locale;
import java.util.function.Function;

/**
 * @program : battleship
 * @description: Class of TextPlayer, for two players' board
 **/

public class TextPlayer {

    private final Board<Character> theBoard;
    private final BoardTextView view;
    private final BufferedReader inputReader;
    private PrintStream out;
    private final AbstractShipFactory<Character> shipFactory;
    private final String name;
    final ArrayList<String> shipsToPlace;
    final HashMap<String, Function<Placement, Ship<Character>>> shipCreationFns;
    private int moveChance;
    private int sonarChance;
    private boolean isRobot;
    private PrintStream robotOut;
    private RandomFactory randomFactory;


    /**
     * @Description: Constructor of TextPlayer. Set default Board, Reader and PrintStream
     * @Param:       Board theBoard is the Board of game ; Reader inputSource is the pipeline of input ; PrintStream out is the pipeline of output
     * @return:
     **/
    public TextPlayer(String name, Board<Character> theBoard, Reader inputSource, PrintStream out, AbstractShipFactory<Character> shipFactory) {
        this.theBoard = theBoard;
        this.view = new BoardTextView(theBoard);
        this.inputReader = new BufferedReader(inputSource);
        this.out = out;
        this.shipFactory = shipFactory;
        this.name = name;

        this.shipsToPlace = new ArrayList<>();
        this.shipCreationFns = new HashMap<>();
        setupShipCreationMap();
        setupShipCreationList();
        this.moveChance = 3;
        this.sonarChance = 3;
        this.isRobot = false;
        this.robotOut = new PrintStream(OutputStream.nullOutputStream());
        this.randomFactory = new RandomFactory(theBoard);
    }

    /** Getters and Setters **/
    public String getName() {
        return name;
    }

    public BoardTextView getView() {
        return view;
    }

    public PrintStream getOut() {
        return out;
    }

    public Board<Character> getTheBoard() {
        return theBoard;
    }

    protected void setupShipCreationMap(){
        shipCreationFns.put("Submarine", (p) -> shipFactory.makeSubmarine(p));
        shipCreationFns.put("Destroyer", (p) -> shipFactory.makeDestroyer(p));
        shipCreationFns.put("Carrier", (p) -> shipFactory.makeCarrier(p));
        shipCreationFns.put("Battleship", (p) -> shipFactory.makeBattleship(p));
    }

    protected void setupShipCreationList(){
        shipsToPlace.addAll(Collections.nCopies(2, "Submarine"));
        shipsToPlace.addAll(Collections.nCopies(3, "Destroyer"));
        shipsToPlace.addAll(Collections.nCopies(3, "Battleship"));
        shipsToPlace.addAll(Collections.nCopies(2, "Carrier"));
    }

    public void setRobot(){
        isRobot = true;
    }

    public void setRobotOut(){
        robotOut = out;
        out = new PrintStream(OutputStream.nullOutputStream());
    }

    /**
     * @Description: Output String prompt and read user's input to create a new Placement
     * @Param:       String prompt
     * @throws       IOException if something wrong in input and output
     * @return:      Placement that created from user input
     **/
    public Placement readPlacement(String prompt) throws IOException {
        out.println(prompt);
        String s = inputReader.readLine();
        return new Placement(s);
    }

    /**
     * @Description: Read from user once and do one placement in Board
     * @Param:
     * @throws       IOException if something wrong in input and output
     * @return:
     **/
    public String doOnePlacement(String shipName, Function<Placement, Ship<Character>> createFn) throws IOException {
        try{
            Placement p = isRobot ? randomFactory.makeRandomPlacement() : readPlacement("Player " + name + " where do you want to place a " + shipName + "?");
            Ship<Character> s = createFn.apply(p);
            String shipPlacementProblem = theBoard.tryAddShip(s);
            if(shipPlacementProblem != null){
                return shipPlacementProblem;
            }
            out.print(view.displayMyOwnBoard());
        }
        catch(IllegalArgumentException illegalArgumentException){
            return "That placement is invalid: it does not have the correct format.";
        }

        return null;
    }

    /**
     * @Description: Output String prompt and read user's input to create a new Coordinate
     * @Param:       String prompt
     * @throws       IOException if something wrong in input and output, IllegalArgumentException if prompt is invalid
     * @return:      Coordinate that created from user input
     **/
    public Coordinate readCoordinate(String prompt) throws IOException {
        out.println(prompt);
        String s = inputReader.readLine();
        return new Coordinate(s);
    }

    /**
     * @Description: Display main choices of the player
     * @Param:
     * @return:
     **/
    private void playOneTurnDisplayChoice(){
        /* Print Hint Message */
        out.println("Possible actions for Player " + name +":");
        out.println("F Fire at a square");
        if(moveChance != 0){
            out.println("M Move a ship to another square (" + moveChance + " remaining)");
        }
        if(sonarChance != 0){
            out.println("S Sonar scan                    (" + sonarChance + " remaining)");
        }
        out.println("Player " + name + ", what would you like to do?");
    }

    /**
     * @Description: Output String prompt and read user's input to create a new Coordinate
     * @Param:
     * @throws       IOException if something wrong in input and output, IllegalArgumentException if prompt is invalid
     * @return:      String that indicate the main choice of a player in one turn.
     **/
    private String playOneTurnMakeChoice() throws IOException {
        if(moveChance == 0 && sonarChance == 0){
            return "F";
        }

        while(true){
            playOneTurnDisplayChoice();
            String choice = isRobot ? randomFactory.makeRandomMainChoice() : inputReader.readLine().toUpperCase(Locale.ROOT);
            if(choice.equals("F")){
                return "F";
            }
            if(choice.equals("M") && moveChance != 0){
                return "M";
            }
            if(choice.equals("S") && sonarChance != 0){
                return "S";
            }
            out.println("[**WARNING**] Invalid choice, please make your choice again!");
        }
    }

    /**
     * @Description: Output String prompt and read user's input to create a new Coordinate
     * @Param:       String prompt
     * @throws       IOException if something wrong in input and output, IllegalArgumentException if prompt is invalid
     * @return:      Coordinate that created from user input
     **/
    public playResEnum playFire(Board<Character> enemyBoard) throws IOException, IllegalArgumentException {
        try{
            Coordinate attack;
            if(isRobot){
                attack = randomFactory.makeRandomCoordinate();
            }
            else{
                out.println("Player " + name + ", where do you want to attack? (If you want to make choice again, input \"quit\")");
                String res = inputReader.readLine();
                if(res.equals("quit")){
                    return playResEnum.BACKTOMAIN;
                }

                attack = new Coordinate(res);
            }

            enemyBoard.fireAt(attack);
            Character hitRes = enemyBoard.whatIsAtForEnemy(attack);
            if(hitRes == 's'){
                out.println("[Good] You hit a submarine!");
                robotOut.println("-------------------------\n Player " + name + " hit your submarine at" + attack +"\n-------------------------");
                return playResEnum.SUCCESS;
            }
            else if (hitRes == 'd') {
                out.println("[Good] You hit a Destroyer!");
                robotOut.println("-------------------------\n Player " + name + " hit your Destroyer at" + attack +"\n-------------------------");
                return playResEnum.SUCCESS;
            }
            else if (hitRes == 'b') {
                out.println("[Good] You hit a Battleship!");
                robotOut.println("-------------------------\n Player " + name + " hit your Battleship at" + attack +"\n-------------------------");
                return playResEnum.SUCCESS;
            }
            else if (hitRes == 'c') {
                out.println("[Good] You hit a Carrier!");
                robotOut.println("-------------------------\n Player " + name + " hit your Carrier at" + attack +"\n-------------------------");
                return playResEnum.SUCCESS;
            }
            else {
                out.println("[Pity] You missed!");
                robotOut.println("-------------------------\n Player " + name + " miss his attack\n-------------------------");
                return playResEnum.SUCCESS;
            }
        }
        catch (IllegalArgumentException illegalArgumentException){
            return playResEnum.FAIL;
        }
    }

    /**
     * @Description: Output String prompt and read user's input to create a new Coordinate
     * @Param:       String prompt
     * @throws       IOException if something wrong in input and output, IllegalArgumentException if prompt is invalid
     * @return:      Coordinate that created from user input
     **/
    public playResEnum playMove() throws IOException, IllegalArgumentException{
        try{
            Coordinate origin;
            Placement  target;
            if(isRobot){
                origin = randomFactory.makeRandomCoordinate();
                target = randomFactory.makeRandomPlacement();
            }
            else{
                out.println("Player " + name + ", which ship do you want to move? (If you want to make choice again, input \"quit\")");
                String res = inputReader.readLine();
                if(res.equals("quit"))      return playResEnum.BACKTOMAIN;

                origin = new Coordinate(res);
                target = readPlacement("Player" + name + ", which place do you want this ship move to?");
            }

            String shipType = theBoard.getShipTypeByCoord(origin);
            if(shipType == null)        return playResEnum.FAIL;

            Ship<Character> newShip = shipCreationFns.get(shipType).apply(target);
            String moveRes = theBoard.tryMoveShip(origin, newShip);
            if(moveRes != null)         return playResEnum.FAIL;

            moveChance--;
            return playResEnum.SUCCESS;
        }
        catch (IllegalArgumentException illegalArgumentException){
            return playResEnum.FAIL;
        }
    }

    /**
     * @Description: Output String prompt and read user's input to create a new Coordinate
     * @Param:       String prompt
     * @throws       IOException if something wrong in input and output, IllegalArgumentException if prompt is invalid
     * @return:      Coordinate that created from user input
     **/
    public playResEnum playSonar(Board<Character> enemyBoard) throws IOException,IllegalArgumentException{
        try{
            Coordinate center;
            if(isRobot) {
                center = randomFactory.makeRandomCoordinate();
            }
            else{
                out.println("Player " + name + ", Input the center of Sonar (If you want to make choice again, input \"quit\")");
                String res = inputReader.readLine();
                if(res.equals("quit"))      return playResEnum.BACKTOMAIN;
                center = new Coordinate(res);
            }

            HashMap<String,Integer> sonarRes = new HashMap<>(){
                {
                    put("Submarine",0); put("Destroyer",0); put("Battleship",0); put("Carrier",0);
                }
            };

            for(int i=center.getRow()-3; i<=center.getRow()+3; i++){
                if(i < 0 || i > enemyBoard.getHeight())     continue;
                int gap = 3 - Math.abs(i-center.getRow());

                for(int j=center.getColumn()-gap ; j<= center.getColumn()+gap; j++){
                    if(j < 0 || j > enemyBoard.getWidth())  continue;
                    String shipType = enemyBoard.getShipTypeByCoord(new Coordinate(i,j));
                    if(shipType != null)    sonarRes.put(shipType,sonarRes.get(shipType)+1);
                }
            }

            out.println("Submarines occupy " + sonarRes.get("Submarine")+ " squares\n" +
                        "Destroyers occupy " + sonarRes.get("Destroyer") + " squares\n"+
                        "Battleships occupy " + sonarRes.get("Battleship") + " squares\n"+
                        "Carriers occupy " + sonarRes.get("Carrier") + " square\n"
                    );

            sonarChance--;
            return playResEnum.SUCCESS;
        }
        catch (IllegalArgumentException illegalArgumentException){
            return playResEnum.FAIL;
        }
    }

    /**
     * @Description: Read from user once and do one attack in enemy's Board
     * @Param:
     * @throws       IOException if something wrong in input and output, IllegalArgumentException if prompt is invalid
     * @return:      String that indicate the result of attacking (null for invalid input, play again!)
     **/
    public void playOneTurn(Board<Character> enemyBoard, BoardTextView enemyView, String enemyName) throws IOException, IllegalArgumentException {
        out.println(view.displayMyBoardWithEnemyNextToIt(enemyView,"Your Ocean","Player "+ enemyName + "'s Ocean"));

        while(true){
            String choice = playOneTurnMakeChoice();
            playResEnum resEnum;
            /* Fire */
            if(choice.equals("F")){
                resEnum = playFire(enemyBoard);

                /* If player prompt a wrong coordinate, let him play fire again */
                while(resEnum == playResEnum.FAIL){
                    out.println("[**WARNING**] Fire center is invalid.");
                    resEnum = playFire(enemyBoard);
                }

                /* If player prompt a "BACKTOMAIN" option, let him make choice again; Otherwise, turn to another player */
                if(resEnum == playResEnum.BACKTOMAIN)   out.println("Player " + name + ", make choice again!");
                else                                    break;
            }

            /* Move */
            else if(choice.equals("M")){
                robotOut.println("-------------------------\n Player " + name + " used a Movement\n-------------------------");
                resEnum = playMove();
                while(resEnum == playResEnum.FAIL){
                    out.println("[**WARNING**] Ship or Movement is invalid.");
                    resEnum = playMove();
                }
                if(resEnum == playResEnum.BACKTOMAIN)   out.println("Player " + name + ", make choice again!");
                else                                    break;
            }

            /* Sonar */
            else{
                robotOut.println("-------------------------\n Player " + name + " used a SonarScan\n-------------------------");
                resEnum = playSonar(enemyBoard);
                while(resEnum == playResEnum.FAIL){
                    out.println("[**WARNING**] Center of Sonar is invalid.");
                    resEnum = playSonar(enemyBoard);
                }
                if(resEnum == playResEnum.BACKTOMAIN)   out.println("Player " + name + ", make choice again!");
                else                                    break;
            }
        }
    }

    /**
     * @Description: Check this player is lose or not
     * @Param:
     * @return:      Boolean that indicate this player lose or not
     **/
    public boolean lose(){
        return theBoard.allShipSunk();
    }
}
