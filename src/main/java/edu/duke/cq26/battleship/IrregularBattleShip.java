package edu.duke.cq26.battleship;

import edu.duke.cq26.battleship.interfaces.ShipDisplayInfo;

import java.util.HashMap;
import java.util.HashSet;

/**
 * @program : ece651-sp20-cq26-battleship
 * @description: Class of irregulat battleship in version 2
 **/

public class IrregularBattleShip<T> extends BasicShip<T>{
    public final String name;

    /**
     * @Description: Constructor of IrregularBattleship. Constructed by the passed corresponding information
     * @Param:       String name, Coordinate upperLeft, Character orientation, T data, T onHit
     * @return:
     **/
    public IrregularBattleShip(String name, Coordinate upperLeft, Character orientation, ShipDisplayInfo<T> myDisplayInfo, ShipDisplayInfo<T> enemyDisplayInfo) {
        super(makeOrderCoords(upperLeft, orientation), myDisplayInfo, enemyDisplayInfo);
        this.name = name;
    }

    /**
     * @Description: Constructor of IrregularBattleship. Constructed by the passed corresponding information
     * @Param:       String name, Coordinate upperLeft, Character orientation, T data, T onHit
     * @return:
     **/
    public IrregularBattleShip(String name, Coordinate upperLeft, Character orientation, T data, T onHit) {
        this(name, upperLeft, orientation, new SimpleShipDisplayInfo<T>(data, onHit),
                new SimpleShipDisplayInfo<T>(null, data));
    }


    @Override
    public String getName() {
        return name;
    }

    /**
     * @Description: Create a HashMap of Coordinates based on the start Coordinate and size of region
     * @Param:       Character orientation
     * @return:      HashMap that includes all <Serial number, Coordinate> in the given region
     **/
    static HashMap<Integer, Coordinate> makeOrderCoords(Coordinate upperLeft, Character orientation){
        HashMap<Integer, Coordinate> orderCoords = new HashMap<>();
        int baseRow = upperLeft.getRow();
        int baseColumn = upperLeft.getColumn();
        int number = 1;

        // Orientation is UP
        if(orientation == 'U'){
            orderCoords.put(number++, new Coordinate(baseRow,baseColumn + 1));
            for(int increment = 0; increment < 3; increment++){
                orderCoords.put(number++, new Coordinate(baseRow + 1, baseColumn + increment));
            }
        }
        // Orientation is RIGHT
        else if(orientation == 'R'){
            orderCoords.put(number++,new Coordinate(baseRow + 1,baseColumn + 1));
            for(int increment = 0; increment < 3; increment++){
                orderCoords.put(number++,new Coordinate(baseRow + increment, baseColumn));
            }
        }
        // Orientation is DOWN
        else if(orientation == 'D'){
            orderCoords.put(number++,new Coordinate(baseRow + 1,baseColumn + 1));
            for(int increment = 2; increment >= 0; increment--){
                orderCoords.put(number++,new Coordinate(baseRow, baseColumn + increment));
            }
        }
        // Orientation is LEFT
        else{
            orderCoords.put(number++,new Coordinate(baseRow + 1,baseColumn));
            for(int increment = 2; increment >= 0; increment--){
                orderCoords.put(number++,new Coordinate(baseRow + increment, baseColumn + 1));
            }
        }

        return orderCoords;
    }
}
