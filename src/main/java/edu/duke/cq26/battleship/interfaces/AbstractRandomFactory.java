package edu.duke.cq26.battleship.interfaces;

import edu.duke.cq26.battleship.Coordinate;
import edu.duke.cq26.battleship.Placement;

public interface AbstractRandomFactory {
    /**
     * Make a Coordinate with random row number and random column number
     * @return the random Coordinate
     */
    public Coordinate makeRandomCoordinate();

    /**
     * Make a Placement with random Coordinate and random Direction
     * @return the random Placement
     */
    public Placement makeRandomPlacement();

    /**
     * Make a Choice with random choose from main choice
     * @return the Random Choice
     */
    public String makeRandomMainChoice();


}
