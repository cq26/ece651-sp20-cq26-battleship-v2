package edu.duke.cq26.battleship.interfaces;

import edu.duke.cq26.battleship.Coordinate;

/**
 * @program : battleship
 * @description: Interface of shipDisplay
 **/

public interface ShipDisplayInfo<T> {
    public T getInfo(Coordinate where, boolean hit);
}
