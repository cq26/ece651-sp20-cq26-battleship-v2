package edu.duke.cq26.battleship.interfaces;

import edu.duke.cq26.battleship.Coordinate;

import java.util.ArrayList;

public interface Board<T> {
    public int getWidth();
    public int getHeight();
    public ArrayList<Ship<T>> getMyShips();

    public String getShipTypeByCoord(Coordinate c);
    public String tryAddShip(Ship<T> toAdd);
    public String tryMoveShip(Coordinate c,Ship<T> toMove);
    public T whatIsAtForSelf(Coordinate where);
    public T whatIsAtForEnemy(Coordinate where);
    public Ship<T> fireAt(Coordinate c);
    public boolean allShipSunk();
}
