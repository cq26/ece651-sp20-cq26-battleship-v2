package edu.duke.cq26.battleship;

import edu.duke.cq26.battleship.interfaces.ShipDisplayInfo;

/**
 * @program : battleship
 * @description: Class of SimpleDisplayShip
 **/

public class SimpleShipDisplayInfo<T> implements ShipDisplayInfo<T> {
    private final T myData;
    private final T onHit;

    /**
     * @Description: Constructor of SimpleShipDisplay. Constructed by the passed corresponding information
     * @Param:       T myData, T onHit
     * @return:
     **/
    public SimpleShipDisplayInfo(T myData, T onHit) {
        this.myData = myData;
        this.onHit = onHit;
    }

    /**
     * @Description: Get information that to be displayed based on the passed Coordinate and whether the passed Coordinate is hit.
     * @Param:       Coordinate where, boolean hit
     * @return:      T (information)
     **/
    @Override
    public T getInfo(Coordinate where, boolean hit) {
        if(hit){
            return onHit;
        }
        else{
            return myData;
        }
    }
}
