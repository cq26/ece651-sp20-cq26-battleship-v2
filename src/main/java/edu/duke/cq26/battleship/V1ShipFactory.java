package edu.duke.cq26.battleship;

import edu.duke.cq26.battleship.interfaces.AbstractShipFactory;
import edu.duke.cq26.battleship.interfaces.Ship;

/**
 * @program : battleship
 * @description: Class of ships in Version 1
 **/

public class V1ShipFactory implements AbstractShipFactory<Character> {

    /**
     * @Description: Factory's basic function to create different types of ships.
     * @Param:       Placement where, int w, int h, char letter, String name
     * @return:      Ship<Character>
     **/
    protected Ship<Character> createShip(Placement where, int w, int h, char letter, String name){
        Character orientation = where.getOrientation();
        if(orientation != 'H' && orientation != 'V'){
            throw new IllegalArgumentException("Orientation of Placement is illegal");
        }

        int width = w, height = h;
        if(orientation == 'H'){
            width = h;
            height = w;
        }
        return new RectangleShip<Character>(name, where.getCoordinate(), width, height,letter,'*');
    }

    @Override
    public Ship<Character> makeSubmarine(Placement where) {
        return createShip(where, 1, 2, 's', "Submarine");
    }

    @Override
    public Ship<Character> makeBattleship(Placement where) {
        return createShip(where, 1, 4, 'b', "Battleship");
    }

    @Override
    public Ship<Character> makeCarrier(Placement where) {
        return createShip(where, 1, 6, 'c', "Carrier");
    }

    @Override
    public Ship<Character> makeDestroyer(Placement where) {
        return createShip(where, 1, 3, 'd', "Destroyer");
    }
}
