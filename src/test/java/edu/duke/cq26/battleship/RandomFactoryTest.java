package edu.duke.cq26.battleship;

import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Collections;

import static org.junit.jupiter.api.Assertions.*;

/**
 * @program : ece651-sp20-cq26-battleship
 * @description: Test of randomFactory
 **/

public class RandomFactoryTest {
    @Test
    void test_random_coordinate(){
        BattleShipBoard<Character> b1 = new BattleShipBoard<Character>(10, 20, 'X');
        RandomFactory randomFactory = new RandomFactory(b1);

        for(int i=0; i<1000; i++){
            Coordinate c = randomFactory.makeRandomCoordinate();
            assertTrue(c.getRow() < b1.getHeight() && c.getRow() >= 0);
            assertTrue(c.getColumn() < b1.getWidth() && c.getColumn() >= 0);
        }
    }

    @Test
    void test_random_placement(){
        BattleShipBoard<Character> b1 = new BattleShipBoard<Character>(10, 20, 'X');
        RandomFactory randomFactory = new RandomFactory(b1);
        ArrayList<Character> shipType = new ArrayList<>(){
            {
                add('H');   add('V');   add('U');   add('D');   add('L');   add('R');
            }
        };

        for(int i=0; i<1000; i++){
            Placement p = randomFactory.makeRandomPlacement();
            assertTrue(shipType.contains(p.getOrientation()));
        }
    }

    @Test
    void test_make_main_choice(){
        BattleShipBoard<Character> b1 = new BattleShipBoard<Character>(10, 20, 'X');
        RandomFactory randomFactory = new RandomFactory(b1);
        ArrayList<String> choices = new ArrayList<>(){
            {
                addAll(Collections.nCopies(10, "F"));   add("M");   add("S");
            }
        };

        for(int i=0; i<1000; i++){
            String p = randomFactory.makeRandomMainChoice();
            assertTrue(choices.contains(p));
        }
    }
}
