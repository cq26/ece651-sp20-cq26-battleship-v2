package edu.duke.cq26.battleship;

import edu.duke.cq26.battleship.interfaces.Ship;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 * @program : battleship
 * @description: Class of Test of NoCollisionRuleChecker
 **/

public class NoCollisionRuleCheckerTest {
    @Test
    void test_no_collision_rule_checker(){
        V1ShipFactory v1ShipFactory = new V1ShipFactory();
        NoCollisionRuleChecker<Character> checker = new NoCollisionRuleChecker<>(null);
        BattleShipBoard<Character> b1 = new BattleShipBoard<Character>(10, 20, 'X');

        /* destroyer 1 */
        Placement v1_1 = new Placement(new Coordinate(1, 2), 'V');
        Ship<Character> dst1 = v1ShipFactory.makeDestroyer(v1_1);
        b1.tryAddShip(dst1);

        /* destroyer 2 */
        Placement v1_2 = new Placement(new Coordinate(1, 3), 'V');
        Ship<Character> dst2 = v1ShipFactory.makeDestroyer(v1_2);
        b1.tryAddShip(dst2);

        /* destroyer 3 */
        Placement v1_3 = new Placement(new Coordinate(1, 4), 'V');
        Ship<Character> dst3 = v1ShipFactory.makeDestroyer(v1_3);
        b1.tryAddShip(dst3);

        /* destroyer 4 - test collision */
        Placement v1_4 = new Placement(new Coordinate(1, 2), 'H');
        Ship<Character> dst4 = v1ShipFactory.makeDestroyer(v1_4);
        assertEquals("That placement is invalid: the ship overlaps another ship.",checker.checkMyRule(dst4,b1));

        /* destroyer 5 - test collision */
        Placement v1_5 = new Placement(new Coordinate(1, 2), 'V');
        Ship<Character> dst5 = v1ShipFactory.makeDestroyer(v1_5);
        assertEquals("That placement is invalid: the ship overlaps another ship.",checker.checkMyRule(dst5,b1));
    }

    @Test
    void test_combine_rules(){
        V1ShipFactory v1ShipFactory = new V1ShipFactory();
        NoCollisionRuleChecker<Character> checker = new NoCollisionRuleChecker<>(new InBoundsRuleChecker<Character>(null));
        BattleShipBoard<Character> b1 = new BattleShipBoard<Character>(10, 20, 'X');

        /* destroyer 1 */
        Placement v1_1 = new Placement(new Coordinate(0, 0), 'V');
        Ship<Character> dst1 = v1ShipFactory.makeDestroyer(v1_1);
        b1.tryAddShip(dst1);

        /* destroyer 2 */
        Placement v1_2 = new Placement(new Coordinate(0, 1), 'V');
        Ship<Character> dst2 = v1ShipFactory.makeDestroyer(v1_2);
        b1.tryAddShip(dst2);

        /* destroyer 3 - cannot add */
        Placement v1_3 = new Placement(new Coordinate(0, 1), 'V');
        Ship<Character> dst3 = v1ShipFactory.makeDestroyer(v1_3);
        b1.tryAddShip(dst3);

        /* destroyer 4 - test bound */
        Placement v1_4 = new Placement(new Coordinate(-1, 1), 'H');
        Ship<Character> dst4 = v1ShipFactory.makeDestroyer(v1_4);
        assertEquals("That placement is invalid: the ship goes off the top of the board.",checker.checkPlacement(dst4,b1));

        /* destroyer 5 - test collision */
        Placement v1_5 = new Placement(new Coordinate(0, 0), 'H');
        Ship<Character> dst5 = v1ShipFactory.makeDestroyer(v1_5);
        assertEquals("That placement is invalid: the ship overlaps another ship.",checker.checkPlacement(dst5,b1));

        /* destroyer 6 - test normal */
        Placement v1_6 = new Placement(new Coordinate(19, 7), 'H');
        Ship<Character> dst6 = v1ShipFactory.makeDestroyer(v1_6);
        assertNull(checker.checkPlacement(dst6,b1));

    }
}
