package edu.duke.cq26.battleship;

import edu.duke.cq26.battleship.Coordinate;
import edu.duke.cq26.battleship.Placement;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class PlacementTest {
    @Test
    public void test_equals() {
        Placement p1 = new Placement("A0V");
        Placement p2 = new Placement("A0V");
        Placement p3 = new Placement("B2V");
        Placement p4 = new Placement("A2V");
        Placement p5 = new Placement("A2v");

        assertEquals(p1,p1);
        assertEquals(p1,p2);
        assertEquals(p4,p5);
        assertNotEquals(p1,p3);
        assertNotEquals(p1,p4);
        assertNotEquals(p3,p4);
        assertTrue(p1.equals(p2));
        assertFalse(p1.equals(p4));
        assertFalse(p1.equals(new Coordinate(0,0)));

    }

    @Test
    public void test_hashCode() {
        Placement p1 = new Placement("A0V");
        Placement p2 = new Placement("A0V");
        Placement p3 = new Placement("B2V");
        Placement p4 = new Placement("A2V");

        assertEquals(p1.hashCode(), p2.hashCode());
        assertNotEquals(p1.hashCode(), p3.hashCode());
        assertNotEquals(p1.hashCode(), p4.hashCode());
    }

    @Test
    void test_string_constructor_valid_cases() {
        Placement p1 = new Placement("A0V");
        assertEquals('V',p1.getOrientation());
        Coordinate c1 = new Coordinate(0, 0);
        assertEquals(c1,p1.getCoordinate());

        Placement p2 = new Placement("B3H");
        assertEquals('H',p2.getOrientation());
        Coordinate c2 = new Coordinate(1, 3);
        assertEquals(c2,p2.getCoordinate());

        Placement p3 = new Placement("T8V");
        assertEquals('V',p3.getOrientation());
        Coordinate c3 = new Coordinate(19, 8);
        assertEquals(c3,p3.getCoordinate());

        Placement p4 = new Placement("J9H");
        assertEquals('H',p4.getOrientation());
        Coordinate c4 = new Coordinate(9, 9);
        assertEquals(c4,p4.getCoordinate());

    }

    @Test
    public void test_string_constructor_error_cases() {
        assertThrows(IllegalArgumentException.class, () -> new Placement("10H"));
        assertThrows(IllegalArgumentException.class, () -> new Placement("AAH"));
        assertThrows(IllegalArgumentException.class, () -> new Placement("@0V"));
        assertThrows(IllegalArgumentException.class, () -> new Placement("[0V"));
        assertThrows(IllegalArgumentException.class, () -> new Placement("A0K"));
        assertThrows(IllegalArgumentException.class, () -> new Placement("A00"));
        assertThrows(IllegalArgumentException.class, () -> new Placement("Z9}"));
        assertThrows(IllegalArgumentException.class, () -> new Placement("A1O"));
        assertThrows(IllegalArgumentException.class, () -> new Placement("A1"));
    }

}
