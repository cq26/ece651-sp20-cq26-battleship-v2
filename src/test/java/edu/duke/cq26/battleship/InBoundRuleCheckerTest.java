package edu.duke.cq26.battleship;

import edu.duke.cq26.battleship.interfaces.Ship;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 * @program : battleship
 * @description: Class of test of InBoundRuleChecker
 **/

public class InBoundRuleCheckerTest {
    @Test
    void test_bound_rule_checker(){
        V1ShipFactory v1ShipFactory = new V1ShipFactory();
        InBoundsRuleChecker<Character> checker = new InBoundsRuleChecker<>(null);
        BattleShipBoard<Character> b1 = new BattleShipBoard<Character>(10, 20, 'X');

        /* destroyer 1 */
        Placement v1_1 = new Placement(new Coordinate(1, 2), 'V');
        Ship<Character> dst1 = v1ShipFactory.makeDestroyer(v1_1);

        /* destroyer 2 */
        Placement v1_2 = new Placement(new Coordinate(-1, 2), 'V');
        Ship<Character> dst2 = v1ShipFactory.makeDestroyer(v1_2);

        /* destroyer 3 */
        Placement v1_3 = new Placement(new Coordinate(1, -1), 'V');
        Ship<Character> dst3 = v1ShipFactory.makeDestroyer(v1_3);

        /* destroyer 4 */
        Placement v1_4 = new Placement(new Coordinate(20, 2), 'V');
        Ship<Character> dst4 = v1ShipFactory.makeDestroyer(v1_4);

        /* destroyer 5 */
        Placement v1_5 = new Placement(new Coordinate(10, 9), 'H');
        Ship<Character> dst5 = v1ShipFactory.makeDestroyer(v1_5);

        assertNull(checker.checkMyRule(dst1,b1));
        assertEquals("That placement is invalid: the ship goes off the top of the board.",checker.checkMyRule(dst2,b1));
        assertEquals("That placement is invalid: the ship goes off the left of the board.",checker.checkMyRule(dst3,b1));
        assertEquals("That placement is invalid: the ship goes off the bottom of the board.",checker.checkMyRule(dst4,b1));
        assertEquals("That placement is invalid: the ship goes off the right of the board.",checker.checkMyRule(dst5,b1));

    }
}
