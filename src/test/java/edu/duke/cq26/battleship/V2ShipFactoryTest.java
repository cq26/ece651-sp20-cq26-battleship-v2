package edu.duke.cq26.battleship;

import edu.duke.cq26.battleship.Coordinate;
import edu.duke.cq26.battleship.Placement;
import edu.duke.cq26.battleship.V1ShipFactory;
import edu.duke.cq26.battleship.V2ShipFactory;
import edu.duke.cq26.battleship.interfaces.Ship;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

/**
 * @program : ece651-sp20-cq26-battleship
 * @description: Class of ship factory test in version 2
 **/

public class V2ShipFactoryTest {

    private void checkShip(Ship<Character> testShip, String expectedName, char expectedLetter, Coordinate... expectedLocs){
        assertEquals(expectedName,testShip.getName());
        for(Coordinate c : expectedLocs){
            assertTrue(testShip.occupiesCoordinates(c));
            assertEquals(expectedLetter,testShip.getDisplayInfoAt(c,true));
        }
    }

    @Test
    void test_make_ships(){
        V2ShipFactory v2ShipFactory = new V2ShipFactory();

        /* Make Destroyer */
        Placement v1_1 = new Placement(new Coordinate(1, 2), 'H');
        Ship<Character> dst1 = v2ShipFactory.makeDestroyer(v1_1);
        checkShip(dst1, "Destroyer", 'd', new Coordinate(1, 2), new Coordinate(1, 3), new Coordinate(1, 4));

        /* Make Submarine */
        Placement v1_2 = new Placement(new Coordinate(1, 2), 'H');
        Ship<Character> sub = v2ShipFactory.makeSubmarine(v1_2);
        checkShip(sub, "Submarine", 's', new Coordinate(1, 2), new Coordinate(1, 3));

        /* Make Battleship */
        Placement v2_2 = new Placement(new Coordinate(1, 2), 'U');
        Ship<Character> battleship = v2ShipFactory.makeBattleship(v2_2);
        checkShip(battleship, "Battleship", 'b', new Coordinate(1, 3), new Coordinate(2, 2),
                                                                            new Coordinate(2, 3), new Coordinate(2, 4));

        /* Make Carrier */
        Placement v2_3 = new Placement(new Coordinate(1, 2), 'L');
        Ship<Character> carrier = v2ShipFactory.makeCarrier(v2_3);
        checkShip(carrier, "Carrier", 'c', new Coordinate(1, 2), new Coordinate(1, 3), new Coordinate(1, 4),
                                                                            new Coordinate(2, 4), new Coordinate(2, 5), new Coordinate(2, 6));
    }


    @Test
    void test_invalid_placement(){
        V2ShipFactory v2ShipFactory = new V2ShipFactory();

        /* Make Destroyer */
        Placement v1_1 = new Placement(new Coordinate(1, 2), 'A');
        assertThrows(IllegalArgumentException.class, () -> v2ShipFactory.makeDestroyer(v1_1));

        /* Make Battleship */
        Placement v1_2 = new Placement(new Coordinate(1, 2), 'H');
        assertThrows(IllegalArgumentException.class, () -> v2ShipFactory.makeBattleship(v1_2));

        /* Make Destroyer */
        Placement v1_3 = new Placement(new Coordinate(1, 2), 'V');
        assertThrows(IllegalArgumentException.class, () -> v2ShipFactory.makeCarrier(v1_3));
    }

}
