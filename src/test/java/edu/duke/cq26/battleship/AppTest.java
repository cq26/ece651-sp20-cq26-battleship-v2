package edu.duke.cq26.battleship;

import edu.duke.cq26.battleship.*;
import edu.duke.cq26.battleship.interfaces.Board;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.parallel.ResourceAccessMode;
import org.junit.jupiter.api.parallel.ResourceLock;
import org.junit.jupiter.api.parallel.Resources;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Objects;

import static org.junit.jupiter.api.Assertions.*;

class AppTest {
    @Test
    @ResourceLock(value = Resources.SYSTEM_OUT, mode = ResourceAccessMode.READ_WRITE)
    void test_main() throws IOException{
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        PrintStream out = new PrintStream(bytes, true);

//        String inputA = new String(Objects.requireNonNull(getClass().getClassLoader().getResourceAsStream("inputA.txt")).readAllBytes());
//        String inputB = new String(Objects.requireNonNull(getClass().getClassLoader().getResourceAsStream("inputB.txt")).readAllBytes());
        String isRobot1 = "c";
        String isRobot2 = "c";

        InputStream expectedStream = getClass().getClassLoader().getResourceAsStream("output.txt");
        assertNotNull(expectedStream);

        /* Remember old pipe */
        InputStream oldIn = System.in;
        PrintStream oldOut = System.out;

        /* Change input and Output */
        try {
            System.setOut(out);
            String[] args = new String[]{isRobot1,isRobot2};
            for(int i=0; i<5; i++){
                App.main(args);
            }
        }
        finally {
            System.setIn(oldIn);
            System.setOut(oldOut);
        }

        /* Read from output and compare Expected and Actual */
//        String expected = new String(expectedStream.readAllBytes());
//        String actual = bytes.toString();
//        assertEquals(expected, actual);
    }

    @Test
    void test_read_player_type() throws IOException {
        Board<Character> b1 = new BattleShipBoard<>(10, 20, 'X');
        Board<Character> b2 = new BattleShipBoard<>(10, 20, 'X');

        BufferedReader inputA = new BufferedReader(new InputStreamReader(System.in));
        BufferedReader inputB = new BufferedReader(new InputStreamReader(System.in));

        boolean isP1Robot;
        boolean isP2Robot;

        V2ShipFactory factory = new V2ShipFactory();

        TextPlayer p1 = new TextPlayer("A", b1, inputA, System.out, factory);
        TextPlayer p2 = new TextPlayer("B", b2, inputB, System.out, factory);

        App app = new App(p1,p2);
        BufferedReader input = new BufferedReader(new StringReader("c\n"));
        BufferedReader input2 = new BufferedReader(new StringReader("h\n"));
        BufferedReader input3 = new BufferedReader(new StringReader("sadasd\nc\n"));
        assertTrue(app.readPlayerType("this is a test",input));
        assertFalse(app.readPlayerType("this is a test",input2));
        assertTrue(app.readPlayerType("this is a test",input3));
    }
}
